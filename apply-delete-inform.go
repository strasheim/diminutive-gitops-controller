package main

import (
	"bytes"
	"log"
	"strings"
	"sync"
	"time"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/dynamic/dynamicinformer"
	"k8s.io/client-go/tools/cache"
)

// installInformer allows the controller to track changes to the manifests
// inside the cluster and react to the changes with reconciliation, if required.
func (c *Controller) installInformer(m *manifest) {
	if _, exist := c.informer[key(m.resource)]; exist {
		return
	}

	factory := dynamicinformer.NewFilteredDynamicSharedInformerFactory(c.dynClient, time.Minute, corev1.NamespaceAll,
		func(listOptions *metav1.ListOptions) { listOptions.LabelSelector = managedBy + "=" + controllerName })
	informer := factory.ForResource(m.resource).Informer()

	mux := &sync.RWMutex{}
	synced := false
	must(informer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		UpdateFunc: func(oldObj, newObj interface{}) {
			mux.RLock()
			defer mux.RUnlock()
			if !synced {
				return
			}
			if detectChange(oldObj.(*unstructured.Unstructured), newObj.(*unstructured.Unstructured)) {
				informLog("change detected on:", oldObj.(*unstructured.Unstructured))
				c.wq.Add(objKey(oldObj.(*unstructured.Unstructured)))
			}
		},
		DeleteFunc: func(obj interface{}) {
			mux.RLock()
			defer mux.RUnlock()
			if !synced {
				return
			}
			informLog("deletion detected on:", obj.(*unstructured.Unstructured))
			c.wq.Add(objKey(obj.(*unstructured.Unstructured)))
		},
	}))

	ch := make(chan struct{})
	go informer.Run(ch)

	isSynced := cache.WaitForCacheSync(ch, informer.HasSynced)
	mux.Lock()
	synced = isSynced
	mux.Unlock()
	c.informer[key(m.resource)] = ch
}

// apply used serverside apply to json patch manifest.jObj into the cluster.
func (c *Controller) apply(m *manifest) error {
	force := true
	_, err := m.client.Patch(c.ctx, m.k8sName, types.ApplyPatchType, m.jObj, metav1.PatchOptions{
		FieldManager:    controllerName,
		Force:           &force,
		FieldValidation: "Strict",
	})
	if err != nil {
		m.applied = false
		return err
	}
	c.installInformer(m)
	m.applied = true
	return nil
}

// delete is the dynamic client-go reverse function of the apply.
func (c *Controller) delete(m *manifest) error {
	md := metav1.DeletePropagationForeground
	err := m.client.Delete(c.ctx, m.k8sName, metav1.DeleteOptions{PropagationPolicy: &md})
	// resources might already been gone
	if errors.IsGone(err) || errors.IsNotFound(err) {
		return nil
	}
	return err
}

// key generates a map key from the groupVersionResource, which is used
// to set map for every type needed by the controllers informers
// this allows to not overpopulate the informers and a clean delete
// if no longer required.
func key(s schema.GroupVersionResource) string {
	return strings.Join([]string{s.Group, s.Version, s.Resource}, "/")
}

// informLog is a wrapper for common log message containing the objects.
func informLog(message string, u *unstructured.Unstructured) {
	log.Println(message,
		u.GetName(),
		u.GetNamespace(),
		u.GetKind(),
	)
}

// detectChange pre-checks the informer which listens on changes to make
// sure those are actual changes of the object. A sync would retrigger
// the event for all objects with the label the controller cares for
// and in those cases the ResourceVersion does not change. Other changes
// of the status of other controllers co-owning the objects should also
// not cause any change event the controller cares for.
func detectChange(objA, objB *unstructured.Unstructured) bool {
	if objA.GetResourceVersion() == objB.GetResourceVersion() {
		return false
	}
	return !bytes.Equal(cleanObj(objA), cleanObj(objB))
}

// cleanObj removes all the fields the controller does not care for on a
// given change and outputs the unstructured object as pure byte slice.
func cleanObj(u *unstructured.Unstructured) []byte {
	cc := u.DeepCopy()
	cc.SetResourceVersion("")
	cc.SetManagedFields(nil)
	cc.SetDeletionTimestamp(nil)
	cc.Object["status"] = nil
	return orDie(cc.MarshalJSON())
}

// objKey is the key used for all manifests and allows to identify a resource.
func objKey(u *unstructured.Unstructured) string {
	return strings.Join([]string{u.GetNamespace(), u.GetName(), u.GetKind()}, "/")
}
