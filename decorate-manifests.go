package main

import (
	"encoding/json"
	"log"
	"strconv"
	"strings"

	"k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer/yaml"
	"k8s.io/client-go/dynamic"
)

const webhook = "WebhookConfiguration"
const APIService = "APIService"
const managedBy = "app.kubernetes.io/managed-by"

const (
	failure    = iota // 0
	global     = iota
	namespaced = iota
	special    = iota
)

// refreshManifests calls out to the git subfunctions, to get the latest state
// if that state is different to the one it already has, it will render all
// manifests and encode the required fields for Controller.objects.
func (c *Controller) refreshManifests() {
	if !c.git.reHashManifests() && c.noCRDChangeMaterialize {
		c.requiresApply = false
		return
	}
	c.requiresApply = true
	c.noCRDChangeMaterialize = true
	c.objects = make(map[string]*manifest) // hopefully that resets what is there
	for _, m := range c.git.manifests {
		obj := &unstructured.Unstructured{}
		_, gvk, err := yaml.NewDecodingSerializer(unstructured.UnstructuredJSONScheme).Decode(m, nil, obj)
		must(err)

		mapping, err := c.apiMapper.RESTMapping(gvk.GroupKind(), gvk.Version)
		if err != nil {
			// the kind is not known yet, cause the CRDs have not been supplied
			// unless the CRD is added no need for a rerun, if the CRD is added
			// noCRDChangeMaterialized is false and this tree is re-run
			continue
		}

		labels := obj.GetLabels()
		if labels == nil {
			labels = make(map[string]string)
		}
		labels[managedBy] = controllerName
		obj.SetLabels(labels)
		obj.SetManagedFields(nil)

		var om manifest
		var dr dynamic.ResourceInterface
		if mapping.Scope.Name() == meta.RESTScopeNameNamespace {
			dr = c.dynClient.Resource(mapping.Resource).Namespace(obj.GetNamespace())
		} else {
			dr = c.dynClient.Resource(mapping.Resource)
		}
		om.client = dr
		om.jObj = orDie(json.Marshal(obj))
		om.order = c.classifyObject(obj)
		om.resource = mapping.Resource
		om.k8sName = obj.GetName()
		om.namespace = obj.GetNamespace()
		c.objects[om.namespace+"/"+om.k8sName+"/"+obj.GetKind()] = &om
	}
}

// classifyObject checks the meta of an K8s object to see when it should be applied.
// Here you would add more custom logic, like exclusion and extra labels/annotations
// for different ordering of resources.
func (c *Controller) classifyObject(obj *unstructured.Unstructured) int {
	if obj.GetNamespace() != "" {
		return namespaced
	}
	if strings.Contains(obj.GetObjectKind().GroupVersionKind().Kind, webhook) ||
		strings.Contains(obj.GetObjectKind().GroupVersionKind().Kind, APIService) {
		return special
	}
	return global
}

// shallowConversion is called on startup when the configmap of the cluster is read
// it will create a shallow version of the manifest object, which is good enough
// for the deletion function.
func (c *Controller) shallowConversion(cm map[string]string) {
	c.installBase = make(map[string]*manifest) // hopefully that resets what is there
	for k, v := range cm {
		items := strings.Split(v, "/")
		keys := strings.Split(k, "/")
		if len(items) != 4 || len(keys) != 3 {
			log.Println("failed to convert", k, v)
			continue
		}
		resource := schema.GroupVersionResource{
			Group:    items[1],
			Version:  items[2],
			Resource: items[3],
		}
		om := manifest{
			applied:   true,
			order:     orDie(strconv.Atoi(items[0])),
			namespace: keys[0],
			k8sName:   keys[1],
			resource:  resource,
		}
		var dr dynamic.ResourceInterface
		if om.namespace != "" {
			dr = c.dynClient.Resource(om.resource).Namespace(om.namespace)
		} else {
			dr = c.dynClient.Resource(om.resource)
		}
		om.client = dr
		c.installBase[k] = &om
	}
}
