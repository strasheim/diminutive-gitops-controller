package main

import (
	"context"
	"errors"
	"io"
	"io/fs"
	"log"
	"os"
	"strings"
	"time"

	"github.com/go-git/go-billy/v5/memfs"
	"github.com/go-git/go-billy/v5/util"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/transport"
	git_ssh "github.com/go-git/go-git/v5/plumbing/transport/ssh"
	"github.com/go-git/go-git/v5/storage/memory"
	"golang.org/x/crypto/ssh"
)

const deploymentKey = "id_rsa"
const gitTimeout = 15 * time.Second

type gitDetails struct {
	tree       *git.Worktree
	repository *git.Repository
	fileList   []string
	manifests  [][]byte
	firstSync  bool
}

// listFiles is a wrapper around the skinYamls filepath walk, while shallow
// it hides the more complicated arguments that never change for this use case.
func (g *gitDetails) listFiles() *gitDetails {
	g.fileList = nil
	must(util.Walk(g.tree.Filesystem, "", g.skinYAMLs))
	g.readManifests()
	return g
}

// skinYAMLs reads in most yaml files under some filesystem root.
// All .yaml files are read from the in-memory filesystem.
func (g *gitDetails) skinYAMLs(path string, _ fs.FileInfo, err error) error {
	if err != nil {
		log.Println("failure accessing file:", path, err)
		return err
	}
	if strings.HasSuffix(path, ".yaml") {
		g.fileList = append(g.fileList, path)
	}
	return nil
}

// injestManifests wraps the file read and the yaml splicing into a single call.
func injestManifests(filename string, vfs *git.Worktree) [][]byte {
	file := orDie(vfs.Filesystem.Open(filename))
	defer file.Close()
	return separateYAML(orDie(io.ReadAll(file)))
}

// readManifests cycles over injected Manifests and puts those into manifests.
func (g *gitDetails) readManifests() {
	g.manifests = nil
	for _, file := range g.fileList {
		g.manifests = append(g.manifests, injestManifests(file, g.tree)...)
	}
}

// syncLatest git pulls the latest from the upstream, with a timeout of 15s.
// Errors here inform about the git state as well as call erorrs and the
// context timeout.
func (g *gitDetails) syncLatest() error {
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, gitTimeout)
	defer cancel()
	return g.tree.PullContext(ctx, &git.PullOptions{Auth: authBySSHKey()})
}

// auth by SSH Key reads the deploy SSH key.
func authBySSHKey() transport.AuthMethod { //nolint - ireturn by upstream code
	sshKey := orDie(os.ReadFile(deploymentKey))
	signer := orDie(ssh.ParsePrivateKey(sshKey))
	return &git_ssh.PublicKeys{User: "git", Signer: signer}
}

// cloneRepository clones the repository and sets gitDetails for the controller.
// firstSync is set to true, to allow the refresh on the first sync to cause
// downstream actions. The function is only every called once and creates the git
// object for the controller.
func cloneRepository() *gitDetails {
	var gd gitDetails
	r := orDie(git.Clone(memory.NewStorage(), memfs.New(), &git.CloneOptions{
		URL:  os.Getenv("repository"),
		Auth: authBySSHKey(),
	}))
	gd.repository = r
	gd.tree = orDie(r.Worktree())
	gd.firstSync = true
	return &gd
}

// reHashManifests checks for the git error code from git.NoErrAlreadyUpToDate.
func (g *gitDetails) reHashManifests() (newManifests bool) {
	err := g.syncLatest()
	if g.firstSync {
		err = nil
		g.firstSync = false
	}
	if err != nil {
		if !errors.Is(err, git.NoErrAlreadyUpToDate) {
			log.Println(err)
		}
		return false
	}
	g.listFiles()
	return true
}
