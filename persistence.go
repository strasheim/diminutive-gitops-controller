package main

import (
	"log"
	"os"
	"strconv"

	"gopkg.in/yaml.v2"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	applyv1 "k8s.io/client-go/applyconfigurations/core/v1"
)

const yamlFilename = "poormansdb.yaml"
const installedManifests = "diminutive-gitops-controller-installedmanifests"

type persistentMap struct {
	Items map[string]string `yaml:"items"`
}

// readPersistentStorage reads the cm with the object list.
// If no configmap is found a nil map is retured, range operations are
// still fine on it, yet accssing any value or adding a value with panic.
func (c *Controller) readPersistentStorage() {
	cm, err := c.client.CoreV1().ConfigMaps(os.Getenv("NAMESPACE")).Get(c.ctx, installedManifests, metav1.GetOptions{})
	if errors.IsNotFound(err) {
		log.Println("no existing installBase found")
		return
	}
	must(err)
	var pMap persistentMap
	must(yaml.Unmarshal([]byte(cm.Data[yamlFilename]), &pMap))
	c.shallowConversion(pMap.Items)
}

// writePersistentStorage write the applied object into a configmap.
// the list is converted to a double list in a form of a map.
func (c *Controller) writePersistentStorage() {
	m := make(map[string]string, len(c.objects))
	for k, v := range c.objects {
		m[k] = strconv.Itoa(v.order) + "/" + v.resource.Group + "/" + v.resource.Version + "/" + v.resource.Resource
	}
	var data persistentMap
	data.Items = m
	cmData, err := yaml.Marshal(data)
	must(err)

	appliedMap := make(map[string]string, 1)
	appliedMap[yamlFilename] = string(cmData)
	force := true
	cm := applyv1.ConfigMap(installedManifests, os.Getenv("NAMESPACE")).WithData(appliedMap)
	must(c.client.CoreV1().ConfigMaps(os.Getenv("NAMESPACE")).Apply(
		c.ctx,
		cm,
		metav1.ApplyOptions{FieldManager: controllerName, Force: force},
	))
	// as last action of an apply loop writes the data to the in-memory storage
	c.installBase = c.objects
}
