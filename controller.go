package main

import (
	"context"
	"log"
	"time"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/discovery"
	memory "k8s.io/client-go/discovery/cached"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/dynamic/dynamicinformer"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/restmapper"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/util/workqueue"
)

const nano2milli = 1000000
const refreshAPIMapper = "#refreshAPI"

// Controller allows to set the many variables needed when dealing
// with Kubernetes objects.
type Controller struct {
	client                 kubernetes.Interface                         // structured k8s client
	dynClient              *dynamic.DynamicClient                       // dynamic client, to deal with all client yamls
	wq                     workqueue.TypedRateLimitingInterface[string] // the workqueue serializes the actions of the controller
	apiMapper              *restmapper.DeferredDiscoveryRESTMapper      // apiMapper is required by the dynamic client to extract the API path
	ctx                    context.Context                              // global controller context, used most sub-http commands run by the controller
	git                    *gitDetails                                  // in-memory storage for all objects related to git interactions
	objects                map[string]*manifest                         // the key [string] is namespace/name/type
	installBase            map[string]*manifest                         // read in from configmap a shallow version of objects
	informer               map[string]chan struct{}                     // list of all informers installed after an object was installed into the cluster
	log                    combinedLog                                  // used to aggregate the log information of a single interation into a one line
	requiresApply          bool                                         // on upstream changes in GIT this is set to true
	noCRDChangeMaterialize bool                                         // if a new CRD was added the internal manifest rendering needs to be re-run
}

type manifest struct {
	client    dynamic.ResourceInterface   // the client is the already refined client, which can directly be used for that resource
	resource  schema.GroupVersionResource // used in the persistent configmap as value and is required for the informer key
	jObj      []byte                      // json encoded manifest ready to apply
	k8sName   string                      // a copy for convience
	namespace string                      // a copy for convience
	order     int                         // order is used for the apply order and in the future for the deletion order
	applied   bool                        // is set for the manifest if the apply was successful and therefore allows for stats
}

// combinedLog reduces the amount of logs into a single line per iteration.
type combinedLog struct {
	item    string
	appTime float64
	delTime float64
	applied int
	deleted int
}

// NewController creates an instance of the controller following the sample
// controller from the k8s project.
func NewController(ctx context.Context) *Controller {
	k8sconfig := clientConfig()
	rateLimiter := workqueue.DefaultTypedControllerRateLimiter[string]()

	c := &Controller{
		client:    kubernetes.NewForConfigOrDie(k8sconfig),
		dynClient: dynamic.NewForConfigOrDie(k8sconfig),
		ctx:       ctx,
		apiMapper: restmapper.NewDeferredDiscoveryRESTMapper(memory.NewMemCacheClient(discovery.NewDiscoveryClientForConfigOrDie(k8sconfig))),
		wq:        workqueue.NewTypedRateLimitingQueue(rateLimiter),
		git:       cloneRepository().listFiles(),
	}
	c.informer = make(map[string]chan struct{})
	c.readPersistentStorage()  // on first call, read the configmap to find existing objects
	c.customResourceInformer() // install informer to know about CRD changes, which need to trigger a apiMapper rehash
	return c
}

// processNextItem is the main of the controller, each interval its called once.
func (c *Controller) processNextItem() bool {
	item, shutDown := c.wq.Get()
	if shutDown {
		return false
	}
	defer c.wq.Done(item)

	if item == refreshAPIMapper {
		defer c.wq.Forget(item) // if another time tick is happening during sync - do not reschedule for it
		c.noCRDChangeMaterialize = false
		c.requiresApply = true
		c.apiMapper = restmapper.NewDeferredDiscoveryRESTMapper(memory.NewMemCacheClient(discovery.NewDiscoveryClientForConfigOrDie(clientConfig())))
		return true
	}

	// pollticks are time ticks from the controller itself and not reactions to events from Kubernetes
	if item == pollTick {
		defer c.wq.Forget(item) // if another time tick is happening during sync - do not reschedule for it
		c.refreshManifests()
		if !c.requiresApply {
			return true
		}
		c.applyAllManifests()
		c.deleteObjectsNoLongerInPresentInGit()
		c.writePersistentStorage()
	} else {
		c.applySelectedManifest(item)
	}
	c.logprinter()
	return true
}

// print prints out the the combined log information into a single line.
func (c *Controller) logprinter() {
	if c.log.deleted == 0 && c.log.applied == 0 {
		return
	}
	manifest := "manifests"
	if c.log.applied == 1 {
		manifest = c.log.item
	}
	if c.log.deleted == 0 && c.log.applied == 1 {
		log.Printf("applied %s in %.3fms\n", manifest, c.log.appTime)
		c.log.applied = 0
		c.log.deleted = 0
		return
	}
	if c.log.deleted == 0 {
		log.Printf("applied %d %s in %.3fms\n", c.log.applied, manifest, c.log.appTime)
		c.log.applied = 0
		c.log.deleted = 0
		return
	}
	log.Printf("applied %d %s in %.3fms and deleted %d in %.3fms\n",
		c.log.applied,
		manifest,
		c.log.appTime,
		c.log.deleted,
		c.log.delTime,
	)

	c.log.applied = 0
	c.log.deleted = 0
}

// applyAllManifests cycles over the unorderedObjects and uses server side
// apply with force to create the objects in kubernetes
// it will print how many objects got applied and how long it took.
func (c *Controller) applyAllManifests() {
	start := time.Now()

	for i := 1; i < 4; i++ {
		for k, obj := range c.objects {
			if i != obj.order {
				continue
			}
			err := c.apply(obj)
			if err != nil {
				c.wq.AddAfter(k, time.Minute) // reapplying single manifest after fail
				log.Println(k, err)
			}
		}
	}
	c.log.appTime = float64(time.Since(start).Nanoseconds()) / nano2milli
	applied := 0
	for k, obj := range c.objects {
		if obj.applied {
			applied++
			c.log.item = k
		}
	}
	c.log.applied = applied
}

// applySelectedManifest is used for all non-sync related apply cases
// it will only target a single resource.
func (c *Controller) applySelectedManifest(objkey string) {
	start := time.Now()
	c.log.applied = 0
	for k, obj := range c.objects {
		if k != objkey {
			continue
		}
		c.log.item = k
		err := c.apply(obj)
		if err != nil {
			c.wq.AddAfter(k, time.Minute) // reapplying single manifest after fail
			log.Println(k, err)
		} else {
			c.log.applied = 1
		}
	}
	c.log.appTime = float64(time.Since(start).Nanoseconds()) / nano2milli
}

// deleteObjectsNoLongerInPresentInGit compares the installBase with the
// applied list and calles c.delete(obj) on all remaining objects.
func (c *Controller) deleteObjectsNoLongerInPresentInGit() {
	if c.installBase == nil {
		return
	}
	start := time.Now()
	c.cleanupInformer()
	deletionList := make(map[string]string, len(c.installBase))
	for key := range c.installBase {
		deletionList[key] = key
	}
	for key, obj := range c.objects {
		if obj.applied {
			delete(deletionList, key)
		}
	}
	for remove := range deletionList {
		err := c.delete(c.installBase[remove])
		if err != nil {
			log.Println(remove, err)
		}
	}

	c.log.delTime = float64(time.Since(start).Nanoseconds()) / nano2milli
	c.log.deleted = len(deletionList)
}

// cleanupInformers removes no longer needed informers. This can be called
// nearly everywhere, as it is based on c.objects alone.
func (c *Controller) cleanupInformer() {
	unique := make(map[string]struct{})
	for remains := range c.objects {
		unique[key(c.objects[remains].resource)] = struct{}{} // this makes a map which has keys like c.informer
	}
	removeList := []string{}
	for informer := range c.informer {
		if _, exist := unique[informer]; !exist {
			removeList = append(removeList, informer) // creates a list of existing informers - unique
		}
	}
	for _, informer := range removeList {
		close(c.informer[informer])
		delete(c.informer, informer)
	}
}

// customResourceInformer is used for the case where kubernetes is extended with
// custom resources, which create new API path. The controller needs to know
// about those as the API mapper otherwise fails to find the right groupkind for
// those custom resources.
func (c *Controller) customResourceInformer() {
	crdResource := schema.GroupVersionResource{Version: "v1", Group: "apiextensions.k8s.io", Resource: "customresourcedefinitions"}
	factory := dynamicinformer.NewFilteredDynamicSharedInformerFactory(c.dynClient, 0, corev1.NamespaceAll, nil)
	informer := factory.ForResource(crdResource).Informer()

	must(informer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(_ interface{}) {
			c.wq.Add(refreshAPIMapper)
		},
		UpdateFunc: func(_, _ interface{}) {
			c.wq.Add(refreshAPIMapper)
		},
	}))

	ch := make(chan struct{})
	go informer.Run(ch)
}

// Run kicks off a controller in its own goroutine
// wait.Until never stops as we don't close the "ch".
func (c *Controller) Run(ch chan struct{}) {
	go wait.Until(c.worker, time.Second, ch)
	<-ch
}

// worker starts off next item in the workqueue.
func (c *Controller) worker() {
	for c.processNextItem() {
	}
}
