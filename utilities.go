package main

import (
	"bytes"
	"context"
	"errors"
	"io"
	"log"
	"os"
	"os/signal"
	"runtime/debug"
	"time"

	goyaml "github.com/go-yaml/yaml"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/tools/leaderelection"
	"k8s.io/client-go/tools/leaderelection/resourcelock"
)

type lead func(ctx context.Context)

// installSigHandler handles the termination request from kubernetes
// its unclear if the cancel() here has a race condition.
func installSigHandler(cancel context.CancelFunc) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		log.Println("handling termination request")
		cancel()
		time.Sleep(time.Second)
		os.Exit(0)
	}()
}

// getNewLock is the resource definition for a lock used non-inline
// in the function runLeaderElection.
func getNewLock() *resourcelock.LeaseLock {
	return &resourcelock.LeaseLock{
		LeaseMeta: metav1.ObjectMeta{
			Name:      controllerName,
			Namespace: os.Getenv("NAMESPACE"),
		},
		Client: kubernetes.NewForConfigOrDie(clientConfig()).CoordinationV1(),
		LockConfig: resourcelock.ResourceLockConfig{
			Identity: os.Getenv("PODNAME"),
		},
	}
}

// runLeaderElection is a wrapper around RunOrDie and adds the required
// configuration to RunOrDie. Nearly all values here are the default values.
func runLeaderElection(ctx context.Context, leader lead, cancel context.CancelFunc) {
	leaderelection.RunOrDie(ctx, leaderelection.LeaderElectionConfig{
		Lock:            getNewLock(),
		LeaseDuration:   15 * time.Second,
		RenewDeadline:   10 * time.Second,
		RetryPeriod:     2 * time.Second,
		ReleaseOnCancel: true,
		Callbacks: leaderelection.LeaderCallbacks{
			OnStartedLeading: func(_ context.Context) {
				leader(ctx)
			},
			OnStoppedLeading: func() {
				cancel()
				log.Println("i lost my leadership, terminating")
			},
			OnNewLeader: func(current_id string) {
				if current_id == os.Getenv("PODNAME") {
					return
				}
				log.Println("new leader is", current_id)
			},
		},
	})
}

// buildsha() uses golang 1.18 build debug information to get the last
// commit sha, wraper call around debug.ReadBuildInfo.
func buildsha() string {
	info, ok := debug.ReadBuildInfo()
	if !ok {
		log.Fatalln("Build info not found")
	}
	for _, v := range info.Settings {
		if v.Key == "vcs.revision" {
			return v.Value
		}
	}
	return "failed to extract from binary"
}

// clientConfig creates the rest config needed for the controller
// Follows the default example: https://pkg.go.dev/k8s.io/client-go/tools/clientcmd
func clientConfig() *rest.Config {
	loadingRules := clientcmd.NewDefaultClientConfigLoadingRules()
	configOverrides := &clientcmd.ConfigOverrides{}
	kubeConfig := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(loadingRules, configOverrides)
	return orDie(kubeConfig.ClientConfig())
}

// separateYAML separate combined yamls into single yamls
// https://gist.github.com/yanniszark/c6f347421a1eeb75057ff421e03fd57c
func separateYAML(resources []byte) [][]byte {
	dec := goyaml.NewDecoder(bytes.NewReader(resources))
	var res [][]byte
	for {
		var value interface{}
		err := dec.Decode(&value)
		if errors.Is(err, io.EOF) {
			break
		}
		must(err)
		res = append(res, orDie(goyaml.Marshal(value)))
	}
	return res
}

// must calls panic, for cases where error reporting is not useful.
func must(r ...any) {
	err := r[len(r)-1]
	if err != nil {
		panic(err.(error)) //nolint:forcetypeassert // it does kinda check for the error
	}
}

// orDie allows for the struct to pass if the error is nil.
// nolint
func orDie[T any](t T, err error) T {
	must(err)
	return t
}
