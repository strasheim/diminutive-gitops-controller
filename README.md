### Diminutive Gitops Controller

A tiny gitops controller, which aims at being installed by the Hashicorp Kubernetes Provider. 

#### Limitations

* no helmchart templating
* no kustomize calling
* no GIT token only deploy keys
* no auto default namespace for objects


#### Container Build

The container does not ship with any SSH known hosts, the known host file has to be supplied by the deployment/POD setup. 


#### Sample Terraform Code
This sample code is aiming at the hashicorp terraform provider which can't do CRDs. 
```
resource "kubernetes_deployment_v1" "diminutive-gitops-controller" {
  metadata {
    name      = "diminutive-gitops-controller"
    namespace = "kube-system"
    labels = {
      "app" = "diminutive-gitops-controller"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        "app" = "diminutive-gitops-controller"
      }
    }
    template {
      metadata {
        labels = {
          "app" = "diminutive-gitops-controller"
        }
      }
      spec {
        volume { 
          name = "gitkey"
          secret {
            secret_name = "diminutive-gitops-controller-key"
          }
        }
        volume {
          name = "sshknownhosts"
          config_map {
            name = "gitops-knownhosts"
          }
        }

        security_context {
        fs_group = "1001"
          seccomp_profile {
            type = "RuntimeDefault"
          }
        }
        container {
          name    = "diminutive-gitops-controller"
          image   = "registry.gitlab.com/strasheim/diminutive-gitops-controller:latest"
          security_context {
            capabilities {
              drop = [ "ALL" ]
            }
            allow_privilege_escalation = "false"
            read_only_root_filesystem = "false"
            run_as_non_root = "true"
            run_as_user = "1000"
          }

          volume_mount {
            name = "gitkey"
            mount_path = "/id_rsa"
            sub_path = "id_rsa"
          }
          volume_mount {
            name = "sshknownhosts"
            mount_path = "/etc/ssh/ssh_known_hosts"
            sub_path = "ssh_known_hosts"
          }

          env {
            name = "repository"
            value = "gitlab.com:strasheim/gitops-data"
          }
          env {
            name  = "PODNAME"
            value_from { 
              field_ref {
                field_path = "metadata.name"
              }
            }
          }
          env {
            name = "NAMESPACE"
            value_from { 
              field_ref {
                field_path = "metadata.namespace"
              }
            } 
          }

          resources {
            requests = {
              cpu = "10m"
              memory = "100Mi"
            }
          }

          termination_message_path   = "/dev/termination-log"
          termination_message_policy = "File"
          image_pull_policy          = "Always"
        }

        restart_policy                   = "Always"
        termination_grace_period_seconds = 2
        dns_policy                       = "ClusterFirst"
        service_account_name             = "diminutive-gitops-controller"
      }
    }
    strategy {
      type = "Recreate"
    }

    revision_history_limit    = 1
    progress_deadline_seconds = 60
  }
}
### The service account for deployment 
resource "kubernetes_service_account_v1" "diminutive-gitops-controller" {
  metadata {
    name = "diminutive-gitops-controller"
    namespace = "kube-system"
  }
}

### RoleBinding to the cluster admin role 
resource "kubernetes_cluster_role_binding_v1" "diminutive-gitops-controller" {
  metadata {
    name = "diminutive-gitops-controller"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "diminutive-gitops-controller"
    namespace = "kube-system"
  }
}

### gitops-knownhosts is the known host files for SSH 
### eg: ssh-keyscan gitlab.com 
### 
resource "kubernetes_config_map_v1" "gitops-known-hosts" {
  metadata {
    name = "gitops-knownhosts"
    namespace = "kube-system"
  }
  data = {
    "ssh_known_hosts" = "gitlab.com ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsj2bNKTBSpIYDEGk9KxsGh3mySTRgMtXL583qmBpzeQ+jqCMRgBqB98u3z++J1sKlXHWfM9dyhSevkMwSbhoR8XIq/U0tCNyokEi/ueaBMCvbcTHhO7FcwzY92WK4Yt0aGROY5qX2UKSeOvuP4D6TPqKF1onrSzH9bx9XUf2lEdWT/ia1NEKjunUqu1xOB/StKDHMoX4/OKyIzuS0q/T1zOATthvasJFoPrAjkohTyaDUz2LN5JoH839hViyEG82yB+MjcFV5MU3N1l1QL3cVUCh93xSaua1N85qivl+siMkPGbO5xR/En4iEY6K2XPASUEMaieWVNTRCtJ4S8H+9"
  }
}

### the secret key, please note that you will need your own key 
### further more you want to have it encrypted in git and bootstrap
### a encryption controller into the cluster before this one 
resouce "kubernetes_secrets_v1" "gitopspkey" {
  metadata {
    name = "diminutive-gitops-controller-key"
    namespace = "kube-system"
  }
  data = {
    "id_rsa" = "${file("${path.module}/id_rsa")}"
  }

}

```

### Prior Art
* [TF Kubernetes Provider](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs)
* [Go-Client Sample Controller](https://github.com/kubernetes/sample-controller)
* [Dynamic Go-Client K8s client](https://ymmt2005.hatenablog.com/entry/2020/04/14/An_example_of_using_dynamic_client_of_k8s.io/client-go)
* [Yaml Splitting](https://gist.github.com/yanniszark/c6f347421a1eeb75057ff421e03fd57c)
* [K8s Leadership Election](https://itnext.io/leader-election-in-kubernetes-using-client-go-a19cbe7a9a85)
* [Dynamic Informers](https://blog.dsb.dev/posts/creating-dynamic-informers/)
