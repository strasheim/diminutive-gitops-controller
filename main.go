package main

import (
	"context"
	"log"
	"time"
)

const gitPollTimer = 10 * time.Second
const controllerName = "diminutive-gitops-controller"
const pollTick = "pollTick"

// setting up the riffraff, eventually trying to become the leader and if so
// the function asLeader is run.
func main() {
	log.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds | log.Lshortfile)

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	installSigHandler(cancel)

	runLeaderElection(ctx, asLeader, cancel)
}

// asLeader is called after the leadership election has succeeded
// the function will never stop.
func asLeader(ctx context.Context) {
	controller := NewController(ctx)
	go controller.Run(make(chan struct{}))

	log.Printf("%s build %s starting controller\n", controllerName, buildsha()[0:8])
	controller.wq.Add(pollTick)
	for range time.Tick(gitPollTimer) {
		controller.wq.Add(pollTick)
	}
}
